import { createStore } from Redux
import { Provider } from "react-redux";
import Reducer1 from "./Reducer1"

const store = createStore(Reducer1);

store.subscribe(() => {
    console.log("state update");
    console.log(store.getState());
})

const addCartAction1 = {
    type: 'ADD_CART',
    item: {"id":"1","createdAt":"2019-09-02T12:58:54.103Z","name":"Rustic Metal Fish","price":"289.00","image":"http://lorempixel.com/640/480/food","stock":65171}
}

const addCartAction2 = {
    type: 'ADD_CART',
    item: {"id":"2","createdAt":"2019-09-02T12:58:54.103Z","name":"Yumiyumiii","price":"450.00","image":"http://lorempixel.com/640/480/food","stock":6171}
}

const addCartAction3 = {
    type: 'ADD_METHOD',
    method: "Boleto"
}

store.dispatch(addCartAction1)
store.dispatch(addCartAction2)
store.dispatch(addCartAction3)