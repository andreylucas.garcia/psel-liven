import React, { Component } from 'react';
import { Link, NavLink} from 'react-router-dom'
import "./Productlist.css"
import { connect } from 'react-redux'

/*Componente de um único produto do projeto*/

class Product extends Component {

    constructor(props){
        super(props);

        this.state = {
            product: this.props.items
        };
    }

    toCart(){
        alert(`${this.props.product.name} adicionado ao carrinho`)
        this.props.product.qtycart = 1
        this.props.product.startprice = parseFloat(this.props.product.price)
        this.props.addToCart(this.props.product)
    }

    buy(){
        alert(`${this.props.product.name} adicionado ao carrinho,\n Redirecionando para seu Carrinho`)
        this.props.addToCart(this.props.product)
    }


    render() {
        return (
                <ul key={this.props.product.id} className="ProductCard">
                    <li><img className="prodImg" src={this.props.product.image}></img></li>
                    <li className="boxItem"> 
                        <h2 className="title"> {this.props.product.name}</h2>
                    </li>
                    <li className="boxItem"> 
                        <h1 className="tprice"><strong>R$ {this.props.product.price}</strong></h1>
                    </li>
                    <li className="boxItem"> 
                        <h3><span className="stock">Em Estoque: </span>{this.props.product.stock} unidades</h3>
                    </li>
                    <li className="boxItem">
                        <div className="buttonsDiv">
                        <Link to="/carrinho">
                            <button className="BuyButton" onClick={item => {
                                this.buy( this.props.Product );
                            }}>Comprar</button>
                            </Link>
                            <button className="BuyButton bt2" onClick={item => {
                                this.toCart({ item });
                            }}>Adicionar ao Carrinho</button>
                            
                            
                        </div>                            
                    </li>
                </ul>   
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addToCart: (item) => {
            dispatch({type: 'ADD_CART', item: item})
        }
    }
 }

export default connect(null,mapDispatchToProps)(Product);