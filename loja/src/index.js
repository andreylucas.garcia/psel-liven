import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Login from "./Login"
import Cadastro from "./Cadastro"
import Carrinho from "./Carrinho"
import ProdutosView from "./ProdutosView"
import Finalizado from "./Finalizado"
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Switch, Route  } from 'react-router-dom'

/*Gerenciador de rotas do projeto usando react router e redux*/

import { createStore } from "redux"
import { Provider } from "react-redux";
import Reducer1 from "./Reducer1"

const store = createStore(Reducer1);

store.subscribe(() => {
  console.log("state update");
  console.log(store.getState());
})


ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
        <Switch>
          <Provider store={store}>
            <Route path="/" exact={true} component={App} />
            <Route path="/login" component={Login} />
            <Route path="/cadastro" component={Cadastro} />
            <Route path="/carrinho" component={Carrinho} />
            <Route path="/produtos" component={ProdutosView} />
            <Route path="/finalizado" component={Finalizado} />
          </Provider>
        </Switch>
    </BrowserRouter>
    
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
