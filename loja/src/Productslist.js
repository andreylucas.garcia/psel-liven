import React, { Component } from "react";
import "./Productlist.css"
import Product from './Product.js'

/*Componente de lista de produtos do projeto*/

class ProductsList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            items: [],
            isLoaded: false,
        }       
    }
    
    

    async componentDidMount(){
        const url =  "https://5d6da1df777f670014036125.mockapi.io/api/v1/product";
        const response = await fetch(url);
        const data = await response.json();
        this.setState({
            items: data,
            isLoaded: true
        })
        
    }

    render() {

        if (this.state.isLoaded == false) {
            return (
                <center>
                    <div>loading...</div>
                </center>
            );
                
          }
      
          if (!this.state.items) {
            return <div>Unexpected Error</div>;
          }

        return (
        <div className="ProductList">
            <div className="ProdDisplay">
                {this.state.items.map(items => (
                    <Product
                        key={items.id} 
                        product={items}
                    ></Product>    
                ))} 
            </div>
   
        </div>
    );
    }
}

export default ProductsList;