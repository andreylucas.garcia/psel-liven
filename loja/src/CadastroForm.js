import React, { Component } from "react";
import './Forms.css';
import avatargirl from './imgs/avatargirl.png'

/*Componente de Formulário de Cadastro do projeto*/

class CadastroForm extends Component{

    constructor(props) {
        super(props);
    
        this.state = {
            nome: '',
            email: '',
            password: '',
            cpf: '',
            birthDate: ''
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange (evt) {
        this.setState({ [evt.target.name]: evt.target.value });
      }

    submitHandler = event => {
        alert("Cadastro Realizado com Sucesso!");
        //event.preventDefault();
    }


    render() {
    return (
    <form className="form" onSubmit={this.submitHandler} action="/carrinho">
        <center>
            <div>
                <h2>Cadastre-se</h2>
            </div>
            <div>
                <img className="img" src={avatargirl} alt="avatargirl"/>
            </div>
        </center>
        <div className="formDiv">
            <label  type="text"  className="formLabel">Nome: </label>
            <input placeholder="Seu nome:" name="nome" type="text" value={this.state.nome} onChange={this.handleChange} className="formInput" required/>
        </div>
        <div className="formDiv">
            <label  type="text"  className="formLabel">Email: </label>
            <input placeholder="Seu email:" name="email" type="email" value={this.state.email} onChange={this.handleChange} className="formInput" required/>
        </div>
        <div className="formDiv">
            <label type="text"  className="formLabel">Senha: </label>
            <input placeholder="Senha:" name="password" type="password" value={this.state.password} onChange={this.handleChange} className="formInput" required/>
        </div>
        <div className="formDiv">
            <label type="text"  className="formLabel">CPF: </label>
            <input placeholder="Seu CPF:" name="cpf" type="text" value={this.state.cpf} onChange={this.handleChange} className="formInput" required/>
        </div>
        <div className="formDiv">
            <label type="text"  className="formLabel">Data de Nascimento: </label>
            <input placeholder="Data de Nascimento" name="birthDate" type="date" value={this.state.birthDate} onChange={this.handleChange} className="formInput" required/>
        </div>
        <center>
            <button className="signUpButton"  type="submit" >Fazer Cadastro</button>
        </center>
        
    </form> 
    );
    }
}

export default CadastroForm;