import React, { Component } from 'react';
import MyNavbar from './MyNavbar.js';
import './App.css';
import bg from './imgs/bg.jpg'

/*Home Page do projeto*/

function App() {
  return (
    <div className="App">
      <MyNavbar></MyNavbar>
      <div className="bgImageWrapper">
        <img className="bgImage" src={bg} alt="background"/>
        <a href="/produtos"><button className="productsButton">Acessar</button></a>
      </div>  
      <div class="footer">
        <p>Andrey Garcia e Liven, 2020</p>  
      </div> 
    </div>
  );
}

export default App;
