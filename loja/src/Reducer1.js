const initState = {
    cart: [],
    totalValue: 0,
    method: 'Boleto',
    cupom: null
}

const Reducer1 = (state = initState, action) => {
    if (action.type == 'ADD_CART'){
        return {
            cart: [...state.cart, action.item],
            totalValue: state.totalValue + parseFloat(action.item.price),
            method: state.method,
            cupom: state.cupom
        }
    }
    else if (action.type == 'ADD_METHOD') {
        return {
            cart: [...state.cart],
            totalValue: state.totalValue,
            method: action.item,
            cupom: state.cupom
        }
    }
    else if (action.type == 'DELETE_ITEM'){
        return {
            cart: action.item,
            totalValue: state.totalValue,
            method: state.method,
            cupom: state.cupom
        }
    }
    else if (action.type == 'SET_TOTAL'){
        return {
            cart: state.cart,
            totalValue: action.item,
            method: state.method,
            cupom: state.cupom
        }
    }
    else if (action.type == 'ADD_CUPOM'){
        return {
            cart: state.cart,
            totalValue: state.totalValue - state.totalValue*(action.item/100),
            method: state.method,
            cupom: action.item
        }
    }
    else{
        return {
            cart: [],
            totalValue: 0,
            method: "Boleto",
            cupom: null
        }
    }
}

export default Reducer1