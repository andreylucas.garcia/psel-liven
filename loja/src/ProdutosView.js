import React, { Component } from "react";
import "./ProdutosView.css";
import MyNavbar from './MyNavbar.js';
import Productslist from "./Productslist.js"

/*Tela de produtos do projeto*/

class ProdutosView extends Component {

    render() {
        return (
        <div className="Produtos">
            <div>
                <MyNavbar></MyNavbar>
            </div>
            
            
            <div>
                <center>
                    <h1>Nossos Produtos</h1>
                </center>
                <Productslist></Productslist>  
            </div>
        </div>
    );
    }
}
export default ProdutosView;