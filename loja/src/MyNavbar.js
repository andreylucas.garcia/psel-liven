import React, { Component } from 'react';
import "./MyNavbar.css"
import { Link, NavLink} from 'react-router-dom'

/*Componente de barra de navegação do projeto*/

class MyNavbar extends Component {
    render() {
        return (
        <div className="MyNavbar">
            <div>
                <header>
                    <Link className="top" to="/"><h1>Loja Vende Tudo <i className="fas fa-shopping-bag"></i></h1></Link>
                </header>
            </div>
            <ul className="nav">
                <li className="myNavItem"> <Link to="/"> Home </Link> </li>
                <li className="myNavItem"> <Link to="/produtos">Produtos </Link></li>
                <li className="myNavItem"> <Link to="/login">Login</Link></li>
                <li className="myNavItem"> <Link to="/cadastro">Cadastro</Link></li>
                <li className="myNavItem"> <Link to="/carrinho">Carrinho <i className="fas fa-shopping-cart"></i></Link></li>
                
            </ul>
        </div>
        

        );
        
    }
}

export default MyNavbar;