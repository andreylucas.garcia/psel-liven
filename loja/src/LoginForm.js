import React, { Component } from "react";
import './Forms.css';
import avatar from './imgs/avatar.png';

/*Componente de Formulário de Cadastro do projeto*/

const user = "andreylucas.garcia@gmail.com"
const passwd = "12345"

class LoginForm extends Component{

    constructor(props) {
        super(props);
    
        this.state = {
            username: '',
            password: ''
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange (evt) {
        this.setState({ [evt.target.name]: evt.target.value });
      }

    submitHandler = event => {
        if(this.state.username != user || this.state.password != passwd){
            alert("Usuário não cadastrado, por favor cadastre-se!");
            event.preventDefault();
        }
        else{
            alert("Login realizado com sucesso!");
        }
    }


    render() {
    return (
        <div>
            <form className="form" onSubmit={this.submitHandler} action="/carrinho">
                <center>
                    <div>
                        <h2>Fazer Login</h2>
                    </div>
                    <div>
                        <img className="img1" src={avatar} alt="avatar"/>
                    </div>
                </center>
                <div className="formDiv">
                    <label type="text"  className="formLabel" >Usuário: </label>
                    <input placeholder="Usuário ou Email" name="username" type="text" value={this.state.username} onChange={this.handleChange} className="formInput"/>
                    <i className="envelope far fa-envelope"></i>
                </div>
                <div className="formDiv senha">
                    <label className="formLabel">Senha: </label>
                    <input placeholder="Senha" name="password"  type="password" value={this.state.password} onChange={this.handleChange} className="formInput" />
                    <i class="password fas fa-lock"></i>
                </div>
                <center>
                    <button className="loginButton" type="submit">Login</button>
                </center>
                
            </form> 
        </div>

    );
    }
}

export default LoginForm;