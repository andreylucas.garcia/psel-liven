import React, { Component } from 'react';
import MyNavbar from './MyNavbar.js';
import './Finalizado.css';
import { connect } from 'react-redux'

/*Tela de compra finalizada do projeto*/

class Finalizado extends Component {

    clearCart(){
        this.clrCart()
        
    }
    
    render() {
        console.log(this.props)
        return (
            <div>
                <div>
                    <MyNavbar></MyNavbar>
                </div>
                <div className="FinalizadoDisplay">
                    <h1>Pedido Finalizado!</h1>
                    <h3>Nós da <span className="loja">Loja Vende Tudo</span> agradecemos sua preferência.<br/>
                    Seu Pedido será enviado assim que o pagamento for confimado, para isso entraremos em contato por email !</h3>
                    <div className="dadosPedido">
                        <h3>Informações do Pedido:</h3>
                        <p>Endereço: Rua das Flores, 123, Apto 64</p>
                        <p>CEP: 13565-854</p>
                        <p>Método de Pagamento: {this.props.data.method}</p>
                        <label className="price">Subtotal: R$ {parseFloat(this.props.data.totalValue).toFixed(2)}</label>
                    </div>
                    <a href="/"><button onClick={this.clearCart} className="homeButton">Retornar à Página Inicial</button></a>
                    
                </div>
                
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        data: state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        clrCart: (item) => {
            dispatch({type: 'CLR_CART', item: item})
        }
    }
 }

export default connect(mapStateToProps,mapDispatchToProps)(Finalizado);