import React, { Component } from "react";
import "./Carrinho.css";
import MyNavbar from './MyNavbar.js';
import { Link, NavLink} from 'react-router-dom'
import { connect } from 'react-redux'

/*Tela de Carrinho do projeto*/

class CartList extends Component {
    constructor(props){
        super(props);

        this.state = {
            product: this.props.productsInCart,
            qty: this.props.qty,
            startprice: this.props.startprice
        };
    }

    handleIncrement(){
        this.setState({
            qty: this.state.qty += 1
        })
        this.state.product.cartqty = this.state.qty /*Carrinho não guarda total de itens comprados*/ 
        console.log(this.state.product.cartqty)     /*Carrinho não guarda total de itens comprados*/ 
        var newProduct = this.state.product
        var price = parseFloat(this.state.startprice)
        price = price*this.state.qty
        newProduct.price = parseFloat(price)
        this.state.product = newProduct
        this.props.HandleClick(price/this.state.qty,"+")
    }

    handleDecrement(){
        if(this.state.qty - 1 > 0){
            this.setState({
                qty: this.state.qty -= 1
            })
            this.state.product.cartqty = this.state.qty /*Carrinho não guarda total de itens comprados*/ 
            console.log(this.state.product.cartqty)     /*Carrinho não guarda total de itens comprados*/ 
            var newProduct = this.state.product
            var price = parseFloat(this.state.startprice)
            price = price*this.state.qty
            newProduct.price = parseFloat(price)
            this.state.product = newProduct
            this.props.HandleClick(price/this.state.qty,"-")
        }

    }

    render() {
        return (
            
            <div>
                <ul key={this.state.product.id} className="CartItemWrapper">
                    <li><img className="CartImg" src={this.state.product.image}></img></li>
                    <li className="CartItem Info">
                        <ul className="ulInfo">
                            <li className="liInfo"><h4 className="title">{this.state.product.name}</h4></li>
                            <li className="liInfo stock">Em Estoque</li>
                            <li className="liInfo frete">Elegível para frete grátis</li>
                        </ul> 
                        <div className="quantity">
                            <div className="btnwrapper">
                                <button className="managerd" onClick={() => this.handleDecrement()} >-</button>
                                <label  type="number">{this.state.qty}</label>
                                <button className="managerp" onClick={() => this.handleIncrement()} >+</button>
                            </div>
                            <button className="remover" onClick={() => 
                            this.props.onDelete(this.state.product,this.state.qty,this.state.startprice)
                            
                            }>Remover</button>
                        </div>
                    </li>
                    <li className="CartItem priceTag"> 
                        <h4 className="price">Preço: R$<span>{this.state.startprice}</span>
                        <h3>Total: R$<span className="fullprice">{parseFloat(this.state.product.price).toFixed(2)}</span></h3>
                        
                        </h4>

                    </li>
                </ul>
            </div>
        );
    }
}



class Carrinho extends Component {
    constructor(props) {
        super(props);
        this.handleDelete= this.handleDelete.bind(this);
        this.HandleClick= this.HandleClick.bind(this);
        this.ClickBoleto= this.ClickBoleto.bind(this);
        this.ClickCartão= this.ClickCartão.bind(this);
        this.ClickTransf= this.ClickTransf.bind(this);
        this.addCupom= this.addCupom.bind(this);
        this.props.productsInCart.method = "Boleto"
    }

    

    handleDelete = (item,qty,startprice) => {
        const itemId =  item.id
        const rmv_price = startprice * parseFloat(qty)
        console.log(rmv_price)
        const items = this.props.productsInCart.cart.filter(prod => prod.id != itemId);
        this.props.delete(items)
        this.props.setTotal(this.props.productsInCart.totalValue - parseFloat(rmv_price))
      };

    HandleClick(price,op){
        if(op === "+"){
            this.props.setTotal(this.props.productsInCart.totalValue + parseFloat(price))
        }
        else {
            this.props.setTotal(this.props.productsInCart.totalValue - parseFloat(price))
        }
        /*document.querySelector(".fullprice").innerHTML = this.props.productsInCart.totalValue*/

    }

    ClickBoleto(){
        this.props.addMethod('Boleto')
        console.log(this.props)
    }

    ClickCartão(){
        this.props.addMethod('Cartão de Crédito')
        console.log(this.props)
    }

    ClickTransf(){
        this.props.addMethod('Trasnferência Bancária')
        console.log(this.props)
    }

    addCupom(){
        var x = document.getElementById("cupom").value;
        if(x === "TUDO10%" && this.props.productsInCart.cupom == null){
            this.props.cupom(10)
        }
        console.log(this.props)

    }

    render() {

        console.log(this.props.productsInCart)
        return (
            <div className="Carrinho">
                <div>
                    <MyNavbar></MyNavbar>
                </div>
                <center>
                    <h1>Carrinho de compras </h1>
                </center>
                <div className="CartDisplay">
                    {this.props.productsInCart.cart.map(productsInCart => (
                        <CartList
                        productsInCart={productsInCart}
                        key={productsInCart.id}
                        id={productsInCart.id}
                        onDelete={this.handleDelete}
                        startprice={productsInCart.price}
                        qty={1}
                        HandleClick={this.HandleClick}
                        ></CartList>              
                        ))}
                    <div>
                        <div className="FinalizarCompra">
                            <div className="FinalWrapper">
                                <div className="metodo">
                                    <label className="mtd">Método de Pagamento: </label>
                                    <select className="mtdlabel" name="pgto" id="cars">
                                        <option onClick={this.ClickBoleto}>Boleto</option>
                                        <option onClick={this.ClickCartão}>Cartão de Crédito</option>
                                        <option onClick={this.ClickTransf}>Transferência Bancária</option>
                                    </select>
                                </div>
                                <div className="cupom">
                                    <label className="mtdlabel">Cupom de Desconto: </label>
                                    <input id="cupom" className="cupominput"/>
                                    <button className="cupombtn" onClick={this.addCupom}>Adicionar Cupom</button>
                                </div>
                                <div>
                                    <label className="tprice">Subtotal: R$ {parseFloat(this.props.productsInCart.totalValue).toFixed(2)}</label>
                                </div>
                                <Link to="/finalizado"><button type="submit" className="FinalizeButton" >Finalizar Compra</button></Link>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
 }
}

const mapStateToProps = (state) => {
    return {
        productsInCart: state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addMethod: (item) => {
            dispatch({type: 'ADD_METHOD', item: item})
        },
        setTotal: (item) => {
            dispatch({type: 'SET_TOTAL', item: item})
        },
        delete: (item) => {
            dispatch({type: 'DELETE_ITEM', item: item})
        },
        cupom: (item) => {
            dispatch({type: 'ADD_CUPOM', item: item})
        },
        
    }
 }

export default connect(mapStateToProps,mapDispatchToProps)(Carrinho);