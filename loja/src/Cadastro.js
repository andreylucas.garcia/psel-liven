import React, { Component } from "react";
import MyNavbar from './MyNavbar.js';
import CadastroForm from './CadastroForm.js'
import "./Cadastro.css";

/*Tela de Cadastro do projeto*/

class Cadastro extends Component {
 render() {
 return (
    <div className="Cadastro">
        <div>
            <MyNavbar></MyNavbar>
        </div>
        <div className="Cadastroform">
            <CadastroForm></CadastroForm>
        </div>
        
    </div>
 );
 }
}
export default Cadastro;