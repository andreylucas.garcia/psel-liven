import React, { Component } from "react";
import MyNavbar from './MyNavbar.js';
import LoginForm from './LoginForm.js'
import './Login.css'

/*Tela de Login do projeto*/

class Login extends Component {
 render() {
 return (
    <div className="Login">
        <div>
            <MyNavbar></MyNavbar>
        </div>
        <div className="formWrapper" >          
            <LoginForm></LoginForm>
        </div>
        
        
    </div>
 );
 }
}
export default Login;